﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Singleton of the Transformation Matrix used to map local coordinates to world coordinates and viceverse.
/// </summary>
public class TransformationMatrix
{
    private static TransformationMatrix _instance;

    /* Meta camera is initialized at axis origin in canonical form. (Identity matrix) */
    public static readonly Vector4 _xVersor = new Vector4(1, 0, 0, 0);
    public static readonly Vector4 _yVersor = new Vector4(0, 1, 0, 0);
    public static readonly Vector4 _zVersor = new Vector4(0, 0, 1, 0);
    public static readonly Vector4 _origin = new Vector4(0, 0, 0, 1);

    /* World coordinates. */
    public Vector4 WorldXVersor { get; private set; }
    public Vector4 WorldYVersor { get; private set; }
    public Vector4 WorldZVersor { get; private set; }
    public Vector4 WorldOrigin  { get; private set; }
    public Quaternion Rotation  { get; private set; }

    private Matrix4x4 _worldToLocalMatrix = new Matrix4x4();

    /// <summary>
    /// Get the matrix to tranform a point (x,y,z,1) or a vector (x,y,z,0) from world reference system to local reference system.
    /// </summary>
    public Matrix4x4 WorldToLocalMatrix { get { return this._worldToLocalMatrix; } }

    /// <summary>
    /// Get the matrix to tranform a point (x,y,z,1) or a vector (x,y,z,0) from local reference system to world reference system.
    /// </summary>
    public Matrix4x4 LocalToWorldMatrix { get; private set; }

    /// <summary>
    /// Check if world coordinates are already setted.
    /// </summary>
    public bool Synchronized { get; private set; }

    /// <summary>
    /// Get the istance of TransformationMatrix.
    /// </summary>
    public static TransformationMatrix Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new TransformationMatrix();
            }
            return _instance;
        }
    }

    private TransformationMatrix()
    {
    }

    /// <summary>
    /// Build TransformationMatrix from new reference system.
    /// </summary>
    /// <param name="xVersor">World X versor.</param>
    /// <param name="yVersor">World Y versor.</param>
    /// <param name="zVersor">World Z versor.</param>
    /// <param name="origin">World origin.</param>
    public void SetWorldBase(Vector4 xVersor, Vector4 yVersor, Vector4 zVersor, Vector4 origin, Quaternion rotation)
    {
        /* Save world versors and origin */
        this.WorldXVersor = xVersor;
        this.WorldYVersor = yVersor;
        this.WorldZVersor = zVersor;
        this.WorldOrigin = origin;

        this.Rotation = rotation;

        /* Build Transformation matrix from world coordinates to local coordinates setting world versors in matrix columns.
           This is correct because local base is canonical base! */
        _worldToLocalMatrix.SetColumn(0, WorldXVersor);
        _worldToLocalMatrix.SetColumn(1, WorldYVersor);
        _worldToLocalMatrix.SetColumn(2, WorldZVersor);
        _worldToLocalMatrix.SetColumn(3, WorldOrigin);

        /* Transformation matrix from local coordinates to world coordinates is simply the inverse. */
        LocalToWorldMatrix = _worldToLocalMatrix.inverse;

        Synchronized = true;
    }

    /// <summary>
    /// Build TransformationMatrix from new reference system.
    /// </summary>
    /// <param name="xVersor">World X versor.</param>
    /// <param name="yVersor">World Y versor.</param>
    /// <param name="zVersor">World Z versor.</param>
    /// <param name="origin">World origin.</param>
    public void SetWorldBase(Vector3 xVersor, Vector3 yVersor, Vector3 zVersor, Vector3 origin, Quaternion rotation)
    {
        this.SetWorldBase(new Vector4(xVersor.x, xVersor.y, xVersor.z, 0), new Vector4(yVersor.x, yVersor.y, yVersor.z, 0),
                          new Vector4(zVersor.x, zVersor.y, zVersor.z, 0), new Vector4(origin.x, origin.y, origin.z, 1), rotation);
    }

    /// <summary>
    /// Transform a point (x,y,z,1) or a vector (x,y,z,0) from world representation to local representation.
    /// </summary>
    /// <param name="vector">The point or vector to transform.</param>
    /// <returns>The new vector. The same vector if world reference system is not defined yet.</returns>
    public Vector4 ToLocal(Vector4 vector)
    {
        return Synchronized ? _worldToLocalMatrix * vector : vector;
    }

    /// <summary>
    /// Transform a point (x,y,z) from world representation to local representation.
    /// NOTE: to tranform a vector you should use ToLocal(Vector4 v) v = (x,y,z,0)
    /// </summary>
    /// <param name="vector">The point to transform.</param>
    /// <returns>The new vector. The same vector if world reference system is not defined yet.</returns>
    public Vector3 ToLocal(Vector3 vector)
    {
        Vector4 result = ToLocal(new Vector4(vector.x, vector.y, vector.z, 1));
        return new Vector3(result.x, result.y, result.z);
    }

    /// <summary>
    /// Transform a point (x,y,z,1) or a vector (x,y,z,0) from local representation to world representation.
    /// </summary>
    /// <param name="vector">The point or vector to transform.</param>
    /// <returns>The new vector. The same vector if world reference system is not defined yet.</returns>
    public Vector4 ToWorld(Vector4 vector)
    {
        return Synchronized ? LocalToWorldMatrix * vector : vector;
    }

    /// <summary>
    /// Transform a point (x,y,z) from local representation to world representation.
    /// NOTE: to tranform a vector you should use ToWorld(Vector4 v) v = (x,y,z,0)
    /// </summary>
    /// <param name="vector">The point to transform.</param>
    /// <returns>The new vector. The same vector if world reference system is not defined yet.</returns>
    public Vector3 ToWorld(Vector3 vector)
    {
        Vector4 result = ToWorld(new Vector4(vector.x, vector.y, vector.z, 1));
        return new Vector3(result.x, result.y, result.z);
    }

    /// <summary>
    /// Place a created game Object in world.
    /// </summary>
    /// <param name="g">The gameobject to place in world</param>
    public void PlaceInWorld(GameObject g)
    {
        if (Synchronized)
        {
            /* Aligning position */
            g.transform.position = ToLocal(g.transform.position);

            /* Rotate object */
            RotateCore(g);

            //TODO: comunicate to mirage framework.
        }
    }

    /// <summary>
    /// Rotate object g from world rotation to local rotation.
    /// </summary>
    /// <param name="g">The gameobject</param>
    /// <param name="wXVersor">World X axis</param>
    /// <param name="wYVersor">World Y axis</param>
    /// <param name="wZVersor">World Z axis</param>
    /// <returns></returns>
    private Quaternion RotateCore(GameObject g)
    {
        /* Recalculate X and Z rotating around _YVersor.
         * This is done in order to make calibration indipendent from camera rotation around Y.
         * This can be done assuming that calibration is vertical */
        Vector3 WorldXVersor = Quaternion.AngleAxis(-Rotation.eulerAngles.y, _yVersor) * this.WorldXVersor;
        Vector3 WorldZVersor = Quaternion.AngleAxis(-Rotation.eulerAngles.y, _yVersor) * this.WorldZVersor;
        Vector3 WorldYVersor = this.WorldYVersor;

        Quaternion old = g.transform.rotation;

        /* Adjust only Y object rotation. X and Z alteration add errors experimentally  */
        g.transform.rotation = Quaternion.Euler(old.eulerAngles.x, old.eulerAngles.y + Rotation.eulerAngles.y, old.eulerAngles.z);
        
        /* Aligning Z axis */
        Vector3 N = Vector3.Cross(WorldZVersor, _zVersor);

        /* N can be zero when z axis define a single line and not a plane.
           We must rotate around another axis; for example x */
        N = N == Vector3.zero ? (Vector3)WorldXVersor : N;

        /* Calculate angle between z axis rotating around N
          zCW = -zWC */
        float zCW = Vector3.SignedAngle(_zVersor, WorldZVersor, N);
        float zWC = Vector3.SignedAngle(WorldZVersor, _zVersor, N);

        /* Rotate around N */
        g.transform.Rotate(N, zWC, Space.World);

        /* Aligning X axis */
        /* Calculate new X vector after the rotation */
        Vector3 worldNewX = Quaternion.AngleAxis(zWC, N) * WorldXVersor;

        /* Calculate angle between another one axis rotating around aligned Z axis; for example x. */
        float xCW = Vector3.SignedAngle(_xVersor, worldNewX, _zVersor);
        float xWC = Vector3.SignedAngle(worldNewX, _xVersor, _zVersor);

        g.transform.Rotate(_zVersor, xWC, Space.World);
        return g.transform.rotation;
    }
}
