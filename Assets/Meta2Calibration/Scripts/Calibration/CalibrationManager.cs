﻿using Meta;
using Meta.Buttons;
using Meta.Interop.Buttons;
using Meta.Plugin;
using System;
using UnityEngine;

/// <summary>
/// Calibration process controller.
/// </summary>
public class CalibrationManager : MonoBehaviour
{
    private Vector3 _origin = Vector3.zero;
    private Vector3 _xVersor = Vector3.zero;
    private Vector3 _yVersor = Vector3.zero;
    private Vector3 _zVersor = Vector3.zero;
    private Quaternion _rotation = Quaternion.identity;

    private CalibrationStages _currentState;
    private CalibrationUI _calibrationUI;
    private GameObject _crosshair;

    public MetaButtonEventBroadcaster _broadcaster;
    private bool _ok_button_pressed = false;
    private bool _no_button_pressed = false;

    /* Object used to make some experiment. To remove */
    private GameObject g;
    public Vector3 WorldOrigin { get { return _origin; } }
    public bool Synchronized { get { return _currentState == CalibrationStages.CalibrationSuccess; } }

    /// <summary>
    /// Bind GUI and crosshair components.
    /// </summary>
    private void Start()
    {
        _calibrationUI = GameObject.FindGameObjectWithTag("GUI").GetComponent<CalibrationUI>();
        _crosshair = GameObject.FindGameObjectWithTag("Crosshair");

        /* Make crosshair invisible unless Slam mapping is complete */
        _crosshair.transform.localScale = Vector3.zero;

        /* Subscribe to MetaButton in order to receive headset button events */
        _broadcaster.Subscribe(ProcessButtonEvent);

        /* On SlamMappingComplete start State Machine */
        GameObject.FindGameObjectWithTag("CameraRig").GetComponent<SlamLocalizer>().onSlamMappingComplete.AddListener(() =>
        {
            SetCurrentState(CalibrationStages.SettingOrigin);
            _crosshair.transform.localScale = Vector3.one;
        });
    }

    /// <summary>
    /// Process a button events.
    /// </summary>
    /// <param name="button"></param>
    public void ProcessButtonEvent(MetaButton button)
    {
        /* ButtonCamera short press confirm an action. ButtonVolume short press cancel an action. */
        if(button.Type == ButtonType.ButtonCamera && button.State == ButtonState.ButtonShortPress)
        {
            _ok_button_pressed = true;
        } else if ((button.Type == ButtonType.ButtonVolumeDown || button.Type == ButtonType.ButtonVolumeUp) && button.State == ButtonState.ButtonShortPress)
        {
            _no_button_pressed = true;
        }
    }

    /// <summary>
    /// Run calibration state machine.
    /// </summary>
    private void Update () {
        switch (_currentState)
        {
            case (CalibrationStages.SettingOrigin):
                /* Need to set the depth occlusion for the calibration */
                SetDepthOcclusion();
                if ((Input.GetKeyDown(KeyCode.S) || _ok_button_pressed) && _crosshair != null)
                {
                    /* Get the position of the crosshair.
                       IN THE FIRST EXECUTION IT MAY BLOCKS EVERYTHING FOR A COUPLE OF SECONDS (WHY?) */
                    _origin = _crosshair.transform.position;

                    /* Get the rotation of camera when setting origin in order to correct rotation of world system of reference */
                    _rotation = Camera.main.transform.rotation;

                    SetCurrentState(CalibrationStages.SettingX);
                }
                break;
            case (CalibrationStages.SettingX):
                if ((Input.GetKeyDown(KeyCode.S) || _ok_button_pressed) && _crosshair != null)
                {
                    /* The x versor is the current position compared to origin. Normalized make this point a versor. */
                    _xVersor = (_crosshair.transform.position - _origin).normalized;
                    SetCurrentState(CalibrationStages.SettingY);
                }
                break;
            case (CalibrationStages.SettingY):
                if((Input.GetKeyDown(KeyCode.S) || _ok_button_pressed) && _crosshair != null)
                {
                    _yVersor = (_crosshair.transform.position - _origin).normalized;
                    SetCurrentState(CalibrationStages.CalibrationEnd);
                }
                break;
            case (CalibrationStages.CalibrationEnd):
                /* Z Versor is obtained with the cross product between Y and X. 
                   Z is perpendicular to the plane created by Y and X */
                _zVersor = Vector3.Cross(_yVersor,_xVersor).normalized;
                /* Y Versor is corrected obtaining the perpendicular Vector to the plane created by X and Z.
                   Doing so axis are orthogonal each other. */
                _yVersor = Vector3.Cross(_xVersor, _zVersor).normalized;
                /* Z must be reversed. Due to left-right handed notation. */
                _zVersor = -_zVersor;

                /* There is a problem, probably because of a bad positioning of axis from user. Need a recalibration.
                   In the current version it should not happen. */
                if (_zVersor.z == 0)
                {
                    Debug.Log("X and Y are parallel. Need to restart calibration process.");
                    SetCurrentState(CalibrationStages.CalibrationFailed);
                }

                /* On cancel input received restart calibration process. */
                if (Input.GetKeyDown(KeyCode.R) || _no_button_pressed)
                {
                    SetCurrentState(CalibrationStages.SettingOrigin);
                }

                /* On confirm input received save the world reference system. */
                if (Input.GetKeyDown(KeyCode.S) || _ok_button_pressed)
                {
                    /* Crosshair is no more helpful. */
                    Destroy(_crosshair);

                    /* Init the Transformation Matrix singleton. */
                    TransformationMatrix.Instance.SetWorldBase(_xVersor, _yVersor, _zVersor, _origin, _rotation);

                    /* Now world is synchronized. */
                    SetCurrentState(CalibrationStages.CalibrationSuccess);
                }
                break;
            case (CalibrationStages.CalibrationFailed):
                if (Input.GetKeyDown(KeyCode.R) || _no_button_pressed)
                {
                    SetCurrentState(CalibrationStages.SettingOrigin);
                }
                break;
            case (CalibrationStages.CalibrationSuccess):
                /* Need to reset depth occlusion at start condition. */
                ResetDepthOcclusion();

                /* Example metods that creates a cube with a rotation and a position specified respect to world system of reference*/
                if (Input.GetKeyDown(KeyCode.C))
                {
                    g = Instantiate((GameObject)Resources.Load("prefabs/cuboNumerato", typeof(GameObject)), new Vector3(0f,0f,0f), Quaternion.Euler(new Vector3(0,0,0)));
                }
                if (Input.GetKeyDown(KeyCode.R))
                {
                    TransformationMatrix.Instance.PlaceInWorld(g);
                }
                break;
        }
    }

    /// <summary>
    /// Change calibration state machine stage and update the GUI.
    /// </summary>
    /// <param name="state">New state.</param>
    private void SetCurrentState(CalibrationStages state)
    {
        _no_button_pressed = false;
        _ok_button_pressed = false;
        _currentState = state;
        _calibrationUI.ChangeUIStage(_currentState);
    }

    /// <summary>
    /// Set depth occlusion on.
    /// </summary>
    private void SetDepthOcclusion()
    {
        var compositor = FindObjectOfType<MetaCompositor>();
        if (compositor)
        {
            compositor.EnableHandOcclusion = true;
        }
    }

    /// <summary>
    /// Set depth occlusion to initial state.
    /// </summary>
    private void ResetDepthOcclusion()
    {
        var compositor = FindObjectOfType<MetaCompositor>();
        if (compositor)
        {
            compositor.EnableHandOcclusion = compositor.OcclusionEnabledAtStart;
        }
    }
}
