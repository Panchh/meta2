﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Enumeration of calibration UI Message types.
/// </summary>
public enum CalibrationUIMessageType
{
    None,
    SettingOrigin,
    SettingXAxis,
    SettingYAxis,
    CalibrationSuccess,
    CalibrationFailed,
}