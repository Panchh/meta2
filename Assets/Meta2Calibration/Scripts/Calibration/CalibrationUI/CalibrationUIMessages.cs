﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class CalibrationUIMessages : MonoBehaviour
{
    [Tooltip("Text field for the message title")]
    [SerializeField]
    private Text _messageTitle;

    [Tooltip("Text field for the message content")]
    [SerializeField]
    private Text _messageContent;

    [Tooltip("Animation time in seconds to fade a message")]
    [SerializeField]
    private float _fadeTime = 0.5f;

    [Tooltip("Current message being displayed")]
    [SerializeField]
    private CalibrationUIMessageType _currentMessage;

    private CalibrationMessage _calibrationMessage;
    private Dictionary<CalibrationUIMessageType, CalibrationMessage> _calibrationUImessages;

    private Color _initialTitleColor;
    private Color _initialContentColor;

    /// <summary>
    /// Current message being displayed.
    /// </summary>
    public CalibrationUIMessageType CurrentMessage
    {
        get { return _currentMessage; }
        set
        {
            if (_currentMessage != value)
            {
                _currentMessage = value;
                StartCoroutine(ChangeMessage());
            }
        }
    }

    /// <summary>
    /// Init messages dictionary.
    /// </summary>
    private void Awake()
    {
        InitMessages();

        _initialTitleColor = _messageTitle.color;
        _initialContentColor = _messageContent.color;
    }

    private void OnValidate()
    {
        if (isActiveAndEnabled)
        {
            StartCoroutine(ChangeMessage());
        }
    }

    /// <summary>
    /// Init messages.
    /// </summary>
    private void InitMessages()
    {
        _calibrationUImessages = new Dictionary<CalibrationUIMessageType, CalibrationMessage>();

        _calibrationUImessages.Add(CalibrationUIMessageType.None,
            new CalibrationMessage("", ""));
        _calibrationUImessages.Add(CalibrationUIMessageType.SettingOrigin,
            new CalibrationMessage("Allineamento con origine", "Muovi la testa fino ad allineare il mirino con il centro del mondo. Premi S per salvare.", Color.gray));
        _calibrationUImessages.Add(CalibrationUIMessageType.SettingXAxis,
            new CalibrationMessage("Calibrazione asse X", "Muovi la testa fino ad allineare il mirino con l'asse X del mondo. Salva con S.", Color.gray));
        _calibrationUImessages.Add(CalibrationUIMessageType.SettingYAxis,
            new CalibrationMessage("Calibrazione asse Y", "Muovi la testa fino ad allineare il mirino con l'asse Y del mondo. Salva con S.", Color.gray));
        _calibrationUImessages.Add(CalibrationUIMessageType.CalibrationSuccess,
            new CalibrationMessage("Calibrazione effettuata", "Premi S per salvare. R per reiniziare la calibrazione.", Color.green));
        _calibrationUImessages.Add(CalibrationUIMessageType.CalibrationFailed,
            new CalibrationMessage("Calibrazione fallita.", "Premi R per ricalibrare", Color.red));
    }

    /// <summary>
    /// Change message to display.
    /// </summary>
    /// <returns></returns>
    private IEnumerator ChangeMessage()
    {
        if (_calibrationUImessages != null)
        {
            if (_calibrationUImessages.TryGetValue(_currentMessage, out _calibrationMessage))
            {
                /* Fade out. */
                yield return Fade(1, 0, _fadeTime);

                if (_messageTitle != null && _messageContent != null)
                {
                    /* Set color. */
                    _messageTitle.color = (_calibrationMessage.TitleColor != null) ? _calibrationMessage.TitleColor.Value : _initialTitleColor;
                    _messageContent.color = (_calibrationMessage.ContentColor != null) ? _calibrationMessage.ContentColor.Value : _initialContentColor;

                    /* Set message. */
                    _messageTitle.text = _calibrationMessage.Title;
                    _messageContent.text = _calibrationMessage.Content;
                }

                /* Fade out. */
                yield return Fade(0, 1, _fadeTime);
            }
            else
            {
                throw new System.Exception("Trying to access a CalibrationUIMessageType key that was not inserted in the dictionary.");
            }
        }
    }

    /// <summary>
    /// Make a fade effect.
    /// </summary>
    /// <param name="start">Initial value.</param>
    /// <param name="end">End value.</param>
    /// <param name="time">Effect duration.</param>
    /// <returns></returns>
    private IEnumerator Fade(float start, float end, float time)
    {
        float initialTime = Time.time;
        while (Time.time - initialTime <= time)
        {
            if (_messageTitle != null && _messageContent != null)
            {
                float alpha = Mathf.Lerp(start, end, (Time.time - initialTime) / time);
                _messageTitle.color = new Color(_messageTitle.color.r, _messageTitle.color.g, _messageTitle.color.b, alpha);
                _messageContent.color = new Color(_messageContent.color.r, _messageContent.color.g, _messageContent.color.b, alpha);
                yield return null;
            }
        }
    }
}
