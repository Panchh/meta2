﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

/// <summary>
/// CalibrationUI controller.
/// </summary>
public class CalibrationUI : MonoBehaviour
{
    private CalibrationUIMessages _calibrationUIMessages;

    /// <summary>
    /// Get the messages controller.
    /// </summary>
    private void Start()
    {
        _calibrationUIMessages = GetComponent<CalibrationUIMessages>();
    }

    /// <summary>
    /// Change the current UI stage based on the calibration process.
    /// </summary>
    /// <param name="calibrationStage"></param>
    public void ChangeUIStage(CalibrationStages calibrationStage)
    {
        switch (calibrationStage)
        {
            case CalibrationStages.SettingOrigin:
                _calibrationUIMessages.CurrentMessage = CalibrationUIMessageType.SettingOrigin;
                break;

            case CalibrationStages.SettingX:
                _calibrationUIMessages.CurrentMessage = CalibrationUIMessageType.SettingXAxis;
                break;

            case CalibrationStages.SettingY:
                _calibrationUIMessages.CurrentMessage = CalibrationUIMessageType.SettingYAxis;
                break;

            case CalibrationStages.CalibrationEnd:
                _calibrationUIMessages.CurrentMessage = CalibrationUIMessageType.CalibrationSuccess;
                break;

            case CalibrationStages.CalibrationFailed:
                _calibrationUIMessages.CurrentMessage = CalibrationUIMessageType.CalibrationFailed;
                break;

            case CalibrationStages.CalibrationSuccess:
                /* End of calibration process. Canvas is no more useful. */
                Destroy(gameObject);
                break;
            default:
                throw new Exception("Calibration stage not implemented: " + calibrationStage);
        }
    }
}
