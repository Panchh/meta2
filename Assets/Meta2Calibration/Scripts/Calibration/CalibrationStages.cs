﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Enumeration of Calibration process stages.
/// </summary>
public enum CalibrationStages
{   SettingOrigin,
    SettingX,
    SettingY,
    CalibrationEnd,
    CalibrationFailed,
    CalibrationSuccess
};
